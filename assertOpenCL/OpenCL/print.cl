/* -*- coding: latin-1 -*- */
/** \file OpenCL/print.cl
 * \brief
 * Helper macros to use printf() function during development
 * when it is available (from OpenCL 1.2).
 *
 * If before OpenCL 1.2 or macro NDEBUG is defined or macro NPRINT is defined
 * then these PRINT*() macros are simply ignored.
 *
 * See examples of use in examples/kernel/example.cl
 *
 * Official printf() function documentation:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/printfFunction.html
 *
 * Piece of assertOpenCL
 * --- GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * --- http://www.opimedia.be/
 * --- September 16, 2018
 */

#ifdef NDEBUG
# ifndef DOXYGEN
#   define NPRINT
# endif
#endif

#if __OPENCL_C_VERSION__ < 120  // CL_VERSION_1_2
# ifndef DOXYGEN
#   define NPRINT
# endif
#endif



#undef PRINT
#undef PRINT1
#undef PRINT2
#undef PRINT3
#undef PRINT4
#undef PRINT5
#undef PRINT6
#undef PRINT7
#undef PRINT8
#undef PRINT9



#ifdef NPRINT
# define PRINT(str)                                ((void)0)

# define PRINT1(format, a)                         ((void)0)
# define PRINT2(format, a, b)                      ((void)0)
# define PRINT3(format, a, b, c)                   ((void)0)
# define PRINT4(format, a, b, c, d)                ((void)0)
# define PRINT5(format, a, b, c, d, e)             ((void)0)
# define PRINT6(format, a, b, c, d, e, f)          ((void)0)
# define PRINT7(format, a, b, c, d, e, f, g)       ((void)0)
# define PRINT8(format, a, b, c, d, e, f, g, h)    ((void)0)
# define PRINT9(format, a, b, c, d, e, f, g, h, i) ((void)0)

#else
/** \brief
 * printf() function without paramters.
 *
 * @return int
 */
# define PRINT(str)                                (printf(str))


/** \brief
 * printf() function with 1 paramter.
 *
 * @return int
 */
# define PRINT1(format, a)                         (printf(format, a))


/** \brief
 * printf() function with 2 paramters.
 *
 * @return int
 */
# define PRINT2(format, a, b)                      (printf(format, a, b))


/** \brief
 * printf() function with 3 paramters.
 *
 * @return int
 */
# define PRINT3(format, a, b, c)                   (printf(format, a, b, c))


/** \brief
 * printf() function with 4 paramters.
 *
 * @return int
 */
# define PRINT4(format, a, b, c, d)                (printf(format, a, b, c, d))


/** \brief
 * printf() function with 5 paramters.
 *
 * @return int
 */
# define PRINT5(format, a, b, c, d, e)             (printf(format, a, b, c, d, e))


/** \brief
 * printf() function with 6 paramters.
 *
 * @return int
 */
# define PRINT6(format, a, b, c, d, e, f)          (printf(format, a, b, c, d, e, f))


/** \brief
 * printf() function with 7 paramters.
 *
 * @return int
 */
# define PRINT7(format, a, b, c, d, e, f, g)       (printf(format, a, b, c, d, e, f, g))


/** \brief
 * printf() function with 8 paramters.
 *
 * @return int
 */
# define PRINT8(format, a, b, c, d, e, f, g, h)    (printf(format, a, b, c, d, e, f, g, h))


/** \brief
 * printf() function with 9 paramters.
 *
 * @return int
 */
# define PRINT9(format, a, b, c, d, e, f, g, h, i) (printf(format, a, b, c, d, e, f, g, h, i))
#endif
