/* -*- coding: latin-1 -*- */
/** \file OpenCL/assert.cl
 * \brief
 * C-like assert alternative (because assert doesn't exist in OpenCL),
 * with necessary helper macros to transmit information to the host.
 *
 * If macro NDEBUG is defined or macro NASSERT is defined
 * then these assert*() macros are simply ignored.
 *
 * The behavior is similar of C assert
 * but instead abort when an assertion failed
 * a message is printed (if at least OpenCL 1.2)
 * and two buffers are used to transmit the information of the first assertion failed
 * to the host program.
 *
 * See examples of use in examples/kernel/example.cl
 *
 * C assert documentation:
 * https://en.cppreference.com/w/c/error/assert
 *
 * Piece of assertOpenCL
 * --- GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * --- http://www.opimedia.be/
 * --- September 16, 2018
 */

#ifdef NDEBUG
# ifndef DOXYGEN
#   define NASSERT
# endif
#endif



#undef ASSERT_DECLARE_UNIQUE_PARAMS
#undef ASSERT_DECLARE_PARAMS
#undef ASSERT_UNIQUE_PARAMS
#undef ASSERT_PARAMS
#undef assert_reset

#undef assert
#undef assert_n
#undef assert_x
#undef assert_n_x

#undef assert_r
#undef assert_n_r
#undef assert_x_r
#undef assert_n_x_r

#undef assert_r0
#undef assert_n_r0
#undef assert_x_r0
#undef assert_n_x_r0

#undef last_assert
#undef last_assert_n
#undef last_assert_x
#undef last_assert_n_x

#undef last_assert_r
#undef last_assert_n_r
#undef last_assert_x_r
#undef last_assert_n_x_r

#undef last_assert_r0
#undef last_assert_n_r0
#undef last_assert_x_r0
#undef last_assert_n_x_r0



#ifdef NASSERT
# define ASSERT_DECLARE_UNIQUE_PARAMS
# define ASSERT_DECLARE_PARAMS
# define ASSERT_UNIQUE_PARAMS
# define ASSERT_PARAMS
# define assert_reset

# define assert(test) ((void)0)
# define assert_n(test, integer_value) ((void)0)
# define assert_x(test, float_value) ((void)0)
# define assert_n_x(test, integer_value, float_value) ((void)0)

# define assert_r(test) ((void)0)
# define assert_n_r(test, integer_value) ((void)0)
# define assert_x_r(test, float_value) ((void)0)
# define assert_n_x_r(test, integer_value, float_value) ((void)0)

# define assert_r0(test) ((void)0)
# define assert_n_r0(test, integer_value) ((void)0)
# define assert_x_r0(test, float_value) ((void)0)
# define assert_n_x_r0(test, integer_value, float_value) ((void)0)

# define last_assert(test) ((void)0)
# define last_assert_n(test, integer_value) ((void)0)
# define last_assert_x(test, float_value) ((void)0)
# define last_assert_n_x(test, integer_value, float_value) ((void)0)

# define last_assert_r(test) ((void)0)
# define last_assert_n_r(test, integer_value) ((void)0)
# define last_assert_x_r(test, float_value) ((void)0)
# define last_assert_n_x_r(test, integer_value, float_value) ((void)0)

# define last_assert_r0(test) ((void)0)
# define last_assert_n_r0(test, integer_value) ((void)0)
# define last_assert_x_r0(test, float_value) ((void)0)
# define last_assert_n_x_r0(test, integer_value, float_value) ((void)0)

#else
/** \brief
 * Declare extra parameters in the signature (without other params)
 * of kernel function or other functions that use assertions.
 *
 * assert_result_ is two int on 64 bits that will contain the line number and the integer value.
 * assert_result_float_ is one float on 32 bits that will contain the floating value.
 */
# define ASSERT_DECLARE_UNIQUE_PARAMS __global unsigned long* const assert_result_, __global float* const assert_result_float_

/** \brief
 * Declare extra parameters in the signature (with other params)
 * of kernel function or other functions that use assertions.
 */
# define ASSERT_DECLARE_PARAMS , ASSERT_DECLARE_UNIQUE_PARAMS


/** \brief
 * Transmit the extra parameters in the calling of a function with ASSERT_DECLARE_UNIQUE_PARAM.
 */
# define ASSERT_UNIQUE_PARAMS assert_result_, assert_result_float_


/** \brief
 * Transmit the extra parameters in the calling of a function with ASSERT_DECLARE_PARAM.
 */
# define ASSERT_PARAMS , ASSERT_UNIQUE_PARAMS


/** \brief
 * Clear previous assertions setting.
 */
# define assert_reset {                         \
    assert_result_[0] = 0;                      \
    assert_result_[1] = 0;                      \
    assert_result_float_[0] = 0.0;              \
    barrier(CLK_GLOBAL_MEM_FENCE);              \
  }



/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value 0 and 0.0,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 */
# define assert(test) assert_n_x(test, 0, 0.0)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value integer_value and 0.0,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define assert_n(test, integer_value) assert_n_x(test, integer_value, 0.0)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value 0 and float_value,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define assert_x(test, float_value) assert_n_x(test, 0, float_value)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value integer_value and float_value,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define assert_n_x(test, integer_value, float_value)   \
  assert_fct_(test, __FILE__, __LINE__, #test,          \
              integer_value, float_value,               \
              assert_result_, assert_result_float_)



/** \brief
 * Like assert() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 */
# define assert_r(test) {                       \
    if (!assert(test)) {                        \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like assert_n() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define assert_n_r(test, integer_value) {      \
    if (!assert_n(test, integer_value)) {       \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like assert_x() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define assert_x_r(test, float_value) {        \
    if (!assert_x(test, float_value)) {         \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like assert_n_x() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define assert_n_x_r(test, integer_value, float_value) {       \
    if (!assert_n_x(test, integer_value, float_value)) {        \
      return;                                                   \
    }                                                           \
  }



/** \brief
 * Like assert() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 */
# define assert_r0(test) {                      \
    if (!assert(test)) {                        \
      return 0;                                 \
    }                                           \
  }


/** \brief
 * Like assert_n() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define assert_n_r0(test, integer_value) {     \
    if (!assert_n(test, integer_value)) {       \
      return 0;                                 \
    }                                           \
  }


/** \brief
 * Like assert_x() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define assert_x_r0(test, float_value) {       \
    if (!assert_x(test, float_value)) {         \
      return 0;                                 \
    }                                           \
  }


/** \brief
 * Like assert_n_x() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define assert_n_x_r0(test, integer_value, float_value) {      \
    if (!assert_n_x(test, integer_value, float_value)) {        \
      return 0;                                                 \
    }                                                           \
  }



/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value 0 and 0.0,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 */
# define last_assert(test) last_assert_n_x(test, 0, 0.0)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value integer_value and 0.0,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define last_assert_n(test, integer_value) last_assert_n_x(test, integer_value, 0.0)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value 0 and float_value,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define last_assert_x(test, float_value) last_assert_n_x(test, 0, float_value)


/** \brief
 * If test is true
 * then do nothing.
 * Else init (if they are still null) assert_result and assert_result_float
 *      with value integer_value and float_value,
 *      and print (if at least OpenCL 1.2) an assertion message.
 *
 * (If assertions are disabled then do nothing.)
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define last_assert_n_x(test, integer_value, float_value)      \
  last_assert_fct_(test, __FILE__, __LINE__, #test,             \
                   integer_value, float_value,                  \
                   assert_result_, assert_result_float_)



/** \brief
 * Like last_assert() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 */
# define last_assert_r(test) {                  \
    if (!last_assert(test)) {                   \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like last_assert_n() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define last_assert_n_r(test, integer_value) { \
    if (!last_assert_n(test, integer_value)) {  \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like last_assert_x() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define last_assert_x_r(test, float_value) {   \
    if (!last_assert_x(test, float_value)) {    \
      return;                                   \
    }                                           \
  }


/** \brief
 * Like last_assert_n_x() but if assertion failed then return.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define last_assert_n_x_r(test, integer_value, float_value) {  \
    if (!last_assert_n_x(test, integer_value, float_value)) {   \
      return;                                                   \
    }                                                           \
  }



/** \brief
 * Like last_assert() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 */
# define last_assert_r0(test) {                 \
    if (!last_assert(test)) {                   \
      return 0;                                 \
    }                                           \
  }


/** \brief
 * Like last_assert_n() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 */
# define last_assert_n_r0(test, integer_value) {        \
    if (!last_assert_n(test, integer_value)) {          \
      return 0;                                         \
    }                                                   \
  }


/** \brief
 * Like last_assert_x() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param float_value
 */
# define last_assert_x_r0(test, float_value) {  \
    if (!last_assert_x(test, float_value)) {    \
      return 0;                                 \
    }                                           \
  }


/** \brief
 * Like last_assert_n_x() but if assertion failed then return 0.
 *
 * Be careful if you have some barriers:
 * https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html
 *
 * @param test the result of the assertion
 * @param integer_value
 * @param float_value
 */
# define last_assert_n_x_r0(test, integer_value, float_value) { \
    if (!last_assert_n_x(test, integer_value, float_value)) {   \
      return 0;                                                 \
    }                                                           \
  }

#endif



#ifndef ASSERT_CL_
#define ASSERT_CL_

/* *******************
 * Private functions *
 *********************/

/** \brief
 * Generic function called by all assert*() macros in debug mode.
 *
 * If test is true
 * then do nothing and return true.
 * Else init (if they are still null) assert_result and assert_result_float,
 *      print (if at least OpenCL 1.2) an assertion message
 *      and return false.
 *
 * @param test the result of the assertion
 * @param filename the filename of the OpenCL file
 * @param line the line number of the assertion in filename to store in assert_result[0]
 * @param assertion the string representation of the assertion checked
 * @param integer_value a integer value to store in assert_result[1]
 * @param float_value a float value to store in assert_result_float[0]
 * @param assert_result
 * @param assert_result_float
 *
 * @return test
 */
bool
assert_fct_(const bool test,
            __constant char* const filename, const unsigned int line,
            __constant char* const assertion,
            const unsigned long integer_value, const float float_value,
            __global unsigned long* const assert_result,
            __global float* const assert_result_float) {
  if (!test) {
    if (assert_result[0] == 0) {
      assert_result[0] = line;
      assert_result[1] = integer_value;
      assert_result_float[0] = float_value;
    }
#if __OPENCL_C_VERSION__ >= 120  // CL_VERSION_1_2
    printf("[%s:%u\tAssertion `%s' failed |\t%lu\t%li |\t%f]\n", filename, line, assertion,
           integer_value, (signed long)integer_value, float_value);
#endif
  }

  return test;
}


/** \brief
 * Generic function called by all last_assert*() macros in debug mode.
 * Similar to assert_fct_() but sets results transmit to the host
 * for each assert that fails instead only the first.
 *
 * If test is true
 * then do nothing and return true.
 * Else set assert_result and assert_result_float,
 *      print (if at least OpenCL 1.2) an assertion message
 *      and return false.
 *
 * @param test the result of the assertion
 * @param filename the filename of the OpenCL file
 * @param line the line number of the assertion in filename to store in assert_result[0]
 * @param assertion the string representation of the assertion checked
 * @param integer_value a integer value to store in assert_result[1]
 * @param float_value a float value to store in assert_result_float[0]
 * @param assert_result
 * @param assert_result_float
 *
 * @return test
 */
bool
last_assert_fct_(const bool test,
                 __constant char* const filename, const unsigned int line,
                 __constant char* const assertion,
                 const unsigned long integer_value, const float float_value,
                 __global unsigned long* const assert_result,
                 __global float* const assert_result_float) {
  if (!test) {
    assert_result[0] = line;
    assert_result[1] = integer_value;
    assert_result_float[0] = float_value;
#if __OPENCL_C_VERSION__ >= 120  // CL_VERSION_1_2
    printf("[%s:%u\tAssertion `%s' failed |\t%lu\t%li\t%f]\n", filename, line, assertion,
           integer_value, (signed long)integer_value, float_value);
#endif
  }

  return test;
}

#endif  // ASSERT_CL_
