#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Parse the include file from OpenCL to extract list of error codes and names.

By default extract from the file:
/usr/include/CL/cl.h

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: September 15, 2018
"""

from __future__ import print_function

import re
import sys


#
# Main
######
def main():
    """
    Main
    """
    filename = (sys.argv[1] if len(sys.argv) > 1
                else '/usr/include/CL/cl.h')

    active = False

    with open(filename) as fin:
        for line in fin:
            line = line.strip()

            if re.match(r'/\*\s*error codes\s*\*/', line.lower()):
                active = True
            elif re.match(r'/\*', line):
                active = False

            if active:
                match = re.match(r'#define\s+(\S+)\s+(\S+)', line)
                if match:
                    print('{{{}, "{}"}},'
                          .format(match.group(2), match.group(1)))


if __name__ == '__main__':
    main()
