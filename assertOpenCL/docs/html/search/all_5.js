var searchData=
[
  ['error_5fname',['error_name',['../example_8c.html#a84964461e35736edf39e242432608ac7',1,'error_name(cl_int code):&#160;example.c'],['../example_8cpp.html#a84964461e35736edf39e242432608ac7',1,'error_name(cl_int code):&#160;example.cpp']]],
  ['error_5fname_5ft',['error_name_t',['../structerror__name__t.html',1,'']]],
  ['errors_5fmap',['errors_map',['../example_8c.html#a422273a702fcda6ff8563d2d93baf222',1,'errors_map():&#160;example.c'],['../example_8cpp.html#ac66a0cb49e1f7f055e3047de87d65eb1',1,'errors_map():&#160;example.cpp']]],
  ['errors_5fmap_5fsize',['errors_map_size',['../example_8c.html#afd885dbd5d2b2b190fb6b9a11791fd65',1,'example.c']]],
  ['example',['Example',['../classExample.html',1,'Example'],['../namespaceexample.html',1,'example'],['../example_8cl.html#a836ba9db2f6279bf91e24984c4ccf69d',1,'example(const unsigned int in, __global unsigned int *const outs ASSERT_DECLARE_PARAMS):&#160;example.cl']]],
  ['example_2ec',['example.c',['../example_8c.html',1,'']]],
  ['example_2ecl',['example.cl',['../example_8cl.html',1,'']]],
  ['example_2ecpp',['example.cpp',['../example_8cpp.html',1,'']]],
  ['example_2ejava',['Example.java',['../Example_8java.html',1,'']]],
  ['example_2epy',['example.py',['../example_8py.html',1,'']]],
  ['example_2escala',['Example.scala',['../Example_8scala.html',1,'']]]
];
