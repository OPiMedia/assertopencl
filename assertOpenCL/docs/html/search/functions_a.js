var searchData=
[
  ['print_5fdevice',['print_device',['../namespaceopencl__infos.html#a25a8581826324b54356aa3b6487a274f',1,'opencl_infos']]],
  ['print_5ferror',['print_error',['../example_8c.html#a18ba83cc7641dfff555e47d9550eaa57',1,'print_error(cl_int code, const char *message):&#160;example.c'],['../example_8cpp.html#a1f4fb4be674045b98a90373126f15779',1,'print_error(cl_int code, std::string message=&quot;&quot;):&#160;example.cpp']]],
  ['print_5fplatform',['print_platform',['../namespaceopencl__infos.html#a7b6b8c45b93850975700af12696fa47b',1,'opencl_infos']]]
];
