/* -*- coding: latin-1 -*- */
/** \file examples/Scala/Example.scala
 * \brief
 * Simple Scala example
 * to show how run a kernel with assert*() and PRINT*() macros
 * and test them.
 *
 * Piece of assertOpenCL
 * --- GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * --- http://www.opimedia.be/
 * --- September 19, 2018
 */

import scala.math.BigInt
import scala.io.Source


import org.jocl.CL._  // http://www.jocl.org/

import org.jocl._



/**
 * Simple Scala example to show how run a kernel with assert*() and PRINT*() macros
 * and test them.
 *
 * Piece of assertOpenCL
 *
 * @author Olivier Pirson --- http://www.opimedia.be/
 * @version September 19, 2018
 */
object Example {
  /** Number of bytes for the float type. */
  val FLOAT_FIELD_SIZE: Int = 4

  /** Number of bytes for the int type. */
  val INT_FIELD_SIZE: Int = 4

  /** Number of bytes for the long type. */
  val LONG_FIELD_SIZE: Int = 8



  /**
   * True iff assertions are enabled.
   */
  private var assertsEnabled: Boolean = false
  try {
    assert(false)
  }
  catch {
    case e: AssertionError => assertsEnabled = true
  }



  /**
   * Read the file and return its content to a string.
   * If failed then throw an exception.
   */
  def fileToString(filename: String): String =
    "#line 1 \"" + filename + "\"\n" + Source.fromFile(filename).mkString


  /**
   * Return the given id device of the given platform OpenCL,
   * or exit if doesn't exists.
   */
  def getDeviceId(platformI: Int, deviceI: Int): cl_device_id = {
    assert(platformI >= 0)
    assert(deviceI >= 0)

    // Get number of platforms
    val platformNbArray: Array[Int] = Array.ofDim[Int](1)

    clGetPlatformIDs(0, null, platformNbArray)

    val platformNb: Int = platformNbArray(0)

    if (platformI >= platformNb) {
      throw new Exception("Wrong platformI: " + platformI)
    }

    // Get all platform ids
    val platformIds: Array[cl_platform_id] = Array.ofDim[cl_platform_id](platformNb)

    clGetPlatformIDs(platformIds.length, platformIds, null)

    // The wanted platform
    val platformId: cl_platform_id = platformIds(platformI)

    // Get number of devices for the wanted platform
    val deviceNbArray: Array[Int] = Array.ofDim[Int](1)

    clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, 0, null, deviceNbArray)

    val deviceNb: Int = deviceNbArray(0)

    if (deviceI >= deviceNb) {
      throw new Exception("Wrong deviceI: " + deviceI)
    }

    // Get all device ids for the wanted platform
    val deviceIds: Array[cl_device_id] = Array.ofDim[cl_device_id](deviceNb)

    clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, deviceNb, deviceIds, null)

    // The wanted device
    deviceIds(deviceI)
  }


  /**
   * Return a string corresponding to device info parameter.
   */
  def getDeviceInfoString(deviceId: cl_device_id, paramName: Int): String = {
    // Get length of result
    val size: Array[Long] = Array.ofDim[Long](1)

    clGetDeviceInfo(deviceId, paramName, 0, null, size)

    // Get the wanted string
    val buffer: Array[Byte] = Array.ofDim[Byte](size(0).toInt)

    clGetDeviceInfo(deviceId, paramName, buffer.length, Pointer.to(buffer), null)

    new String(buffer, 0, buffer.length - 1)
  }


  /**
   * Run the kernel ../kernel/example.cl.

   * If debug
   * then run the kernel in debug mode,
   * else run the kernel with the macro NDEBUG defined.
   */
  def runExample(nbWorkGroup: Int, nbWorkItemsByWorkGroup: Int,
                 deviceId: cl_device_id, debug: Boolean) {
    // Host buffer
    val hOuts: Array[Int] = Array.ofDim[Int](2)
    val hOutsByteSize: Int = INT_FIELD_SIZE * 2

    val hAsserts: Array[Long] = Array[Long](0, 0)
    val hAssertsByteSize: Int = LONG_FIELD_SIZE * 2

    val hAssertFloat: Array[Float] = Array[Float](0)
    val hAssertFloatByteSize: Int = FLOAT_FIELD_SIZE


    // OpenCL context
    val devicesIds: Array[cl_device_id] = Array[cl_device_id](deviceId)
    val context: cl_context = clCreateContext(null, 1, devicesIds, null, null, null)


    // OpenCL kernel
    var options: String = "-I../../OpenCL/"

    if (debug) {
      System.err.println("OpenCL in DEBUG mode!")
      System.err.flush()
    } else {
      options += " -DNDEBUG"
    }

    val kernelFilename: String = "../kernel/example.cl"
    val kernelSrc: String = fileToString(kernelFilename)
    val program: cl_program = clCreateProgramWithSource(context,
                                                        1, Array[String](kernelSrc),
                                                        null, null)

    clBuildProgram(program, 1, devicesIds, options, null, null)

    val kernel: cl_kernel = clCreateKernel(program, "example", null)


    // OpenCL queue
    val queue:cl_command_queue = clCreateCommandQueue(context, deviceId,
                                                      CL_QUEUE_PROFILING_ENABLE, null)


    // OpenCL buffers
    val dOuts: cl_mem = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR,
                                       hOutsByteSize, Pointer.to(hOuts), null)

    var dAsserts: cl_mem = null
    var dAssertFloat: cl_mem = null

    if (debug) {
      dAsserts = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                hAssertsByteSize, Pointer.to(hAsserts), null)
      dAssertFloat = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                    hAssertFloatByteSize, Pointer.to(hAssertFloat), null)
    }


    // Params: just two parameters for the example
    clSetKernelArg(kernel, 0, Sizeof.cl_uint, Pointer.to(Array[Int](666)))
    clSetKernelArg(kernel, 1, Sizeof.cl_mem, Pointer.to(dOuts))

    if (debug) {  // extra parameters to receive assertion information
      val nbArgs: Int = 2

      clSetKernelArg(kernel, nbArgs, Sizeof.cl_mem, Pointer.to(dAsserts))
      clSetKernelArg(kernel, nbArgs + 1, Sizeof.cl_mem, Pointer.to(dAssertFloat))
    }


    // Run
    val globalSize: Int = nbWorkItemsByWorkGroup * nbWorkGroup

    println("===== run kernel =====")
    System.out.flush()

    clEnqueueNDRangeKernel(queue, kernel,
                           1,
                           null,
                           Array[Long](globalSize),
                           Array[Long](nbWorkItemsByWorkGroup),
                           0,
                           null,
                           null)

    clFinish(queue)
    clFlush(queue)
    System.out.flush()
    System.err.flush()
    println("===== end kernel =====")
    System.out.flush()


    // Results
    clEnqueueReadBuffer(queue, dOuts, CL_TRUE, 0,
                        hOutsByteSize, Pointer.to(hOuts), 0, null, null)

    if (debug) {
      clEnqueueReadBuffer(queue, dAsserts, CL_TRUE, 0,
                          hAssertsByteSize, Pointer.to(hAsserts), 0, null, null)
      clEnqueueReadBuffer(queue, dAssertFloat, CL_TRUE, 0,
                          hAssertFloatByteSize, Pointer.to(hAssertFloat), 0, null, null)

      val line: Long = hAsserts(0)

      if (line != 0) {  // there had (at least) an assertion
        val uint64Value: BigInt = uint64(hAsserts(1))
        val sint64Value: Long = hAsserts(1)
        val floatValue: Float = hAssertFloat(0)

        System.err.println(s"$kernelFilename:$line\tAssertion failed | Maybe\t$uint64Value\t$sint64Value | Maybe\t$floatValue")
        /*
          Maybe incoherent assert information because the parallel execution of work items.
          But each element of these information concern assertion(s) that failed.
        */
        System.err.flush()
      }
    }

    println(s"Results: (${uint32(hOuts(0))}, ${uint32(hOuts(1))})")


    // Free OpenCL resources
    clReleaseMemObject(dOuts)
    if (debug) {
      clReleaseMemObject(dAsserts)
      clReleaseMemObject(dAssertFloat)
    }

    clReleaseCommandQueue(queue)
    clReleaseProgram(program)
    clReleaseKernel(kernel)
    clReleaseContext(context)
  }


  /**
   * Return in a Long the value of n considered as an unsigned integer on 32 bits.
   */
  def uint32(n: Int): Long =
    if (n >= 0) n
    else (4294967296L + n)  // 2^32 + n


  /**
   * Return in a BigInt the value of n considered as an unsigned integer on 64 bits.
    */
  def uint64(n: Long): BigInt =
    if (n >= 0) BigInt(n)
    else BigInt("18446744073709551616") + n  // 2^64 + n



  /**
   * Get the optional parameter --device platform:device
   * and run the kernel ../kernel/example.cl .
   */
  def main(args: Array[String]): Unit = {
    var debug: Boolean = assertsEnabled
    var deviceI: Int = 0
    var platformI: Int = 0

    // Read parameters
    {
      var i: Int = 0

      while (i < args.length) {
        val arg: String = args(i)

        if ("--debug".equals(arg) || "--ndebug".equals(arg)) {
          debug = "--debug".equals(arg)
        }
        else if ("--device".equals(arg)) {
          i += 1

          if (i >= args.length) {
            throw new Exception("Missing parameter")
          }

          val bothI: Array[String] = args(i).split(":")

          platformI = Integer.parseInt(bothI(0))
          if (bothI.length >= 2) {
            deviceI = Integer.parseInt(bothI(1))
          }

          if ((platformI < 0) || (deviceI < 0)) {
            throw new Exception("Wrong parameter: " + args(i))
          }
        }
        i += 1
      }
    }

    // Get wanted device
    val deviceId: cl_device_id = getDeviceId(platformI, deviceI)

    // Get device name and print
    println(s"Device $platformI:$deviceI ${getDeviceInfoString(deviceId, CL_DEVICE_NAME)}")
    System.out.flush()

    // Run
    runExample(3, 4, deviceId, debug)
  }
}
