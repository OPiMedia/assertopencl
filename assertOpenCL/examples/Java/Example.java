/* -*- coding: latin-1 -*- */
/** \file examples/Java/Example.java
 * \brief
 * Simple Java (at least since Java 1.8) example
 * to show how run a kernel with assert*() and PRINT*() macros
 * and test them.
 *
 * Piece of assertOpenCL
 * --- GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * --- http://www.opimedia.be/
 * --- September 19, 2018
 */

import java.math.BigInteger;

import java.nio.file.Files;
import java.nio.file.Paths;


import static org.jocl.CL.*;  // http://www.jocl.org/

import org.jocl.*;



/** \brief
 * Simple Java example to show how run a kernel with assert*() and PRINT*() macros
 * and test them.
 */
public class Example {

  /** \brief Number of bytes for the float type. */
  static final int FLOAT_FIELD_SIZE = 4;

  /** \brief Number of bytes for the int type. */
  static final int INT_FIELD_SIZE = 4;

  /** \brief Number of bytes for the long type. */
  static final int LONG_FIELD_SIZE = 8;

  static {
    assert Float.BYTES == FLOAT_FIELD_SIZE;
    assert Integer.BYTES == INT_FIELD_SIZE;
    assert Long.BYTES == LONG_FIELD_SIZE;

    assert Sizeof.cl_float == FLOAT_FIELD_SIZE;
    assert Sizeof.cl_int == INT_FIELD_SIZE;
    assert Sizeof.cl_long == LONG_FIELD_SIZE;
  }



  /** \brief
   * True iff assertions are enabled.
   */
  private static boolean assertsEnabled = false;

  static {
    assert assertsEnabled = true;  // intentional side-effect
  }



  /** \brief
   * Read the file and return its content to a string.
   * If failed then throw an exception.
   */
  static String fileToString(final String filename) throws Exception {
    return ("#line 1 \"" + filename + "\"\n"
            + new String(Files.readAllBytes(Paths.get(filename))));
  }


  /** \brief
   * Return the given id device of the given platform OpenCL,
   * or exit if doesn't exists.
   */
  static cl_device_id getDeviceId(int platformI, int deviceI) throws Exception {
    assert platformI >= 0;
    assert deviceI >= 0;

    // Get number of platforms
    final int[] platformNbArray = new int[1];

    clGetPlatformIDs(0, null, platformNbArray);

    final int platformNb = platformNbArray[0];

    if (platformI >= platformNb) {
      throw new Exception("Wrong platformI: " + platformI);
    }

    // Get all platform ids
    final cl_platform_id[] platformIds = new cl_platform_id[platformNb];

    clGetPlatformIDs(platformIds.length, platformIds, null);

    // The wanted platform
    final cl_platform_id platformId = platformIds[platformI];

    // Get number of devices for the wanted platform
    final int[] deviceNbArray = new int[1];

    clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, 0, null, deviceNbArray);

    final int deviceNb = deviceNbArray[0];

    if (deviceI >= deviceNb) {
      throw new Exception("Wrong deviceI: " + deviceI);
    }

    // Get all device ids for the wanted platform
    final cl_device_id[] deviceIds = new cl_device_id[deviceNb];

    clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, deviceNb, deviceIds, null);

    // The wanted device
    return deviceIds[deviceI];
  }


  /** \brief
   * Return a string corresponding to device info parameter.
   */
  static String getDeviceInfoString(cl_device_id deviceId, int paramName) {
    // Get length of result
    final long[] size = new long[1];

    clGetDeviceInfo(deviceId, paramName, 0, null, size);

    // Get the wanted string
    final byte[] buffer = new byte[(int) size[0]];

    clGetDeviceInfo(deviceId, paramName, buffer.length, Pointer.to(buffer), null);

    return new String(buffer, 0, buffer.length - 1);
  }


  /** \brief
   * Run the kernel ../kernel/example.cl.

   * If debug
   * then run the kernel in debug mode,
   * else run the kernel with the macro NDEBUG defined.
   */
  static void runExample(int nbWorkGroup, int nbWorkItemsByWorkGroup,
                         cl_device_id deviceId, boolean debug) throws Exception {
    // Host buffer
    final int[] hOuts = new int[2];
    final int hOutsByteSize = INT_FIELD_SIZE * 2;

    final long[] hAsserts = {0, 0};
    final int hAssertsByteSize = LONG_FIELD_SIZE * 2;

    final float[] hAssertFloat = {0};
    final int hAssertFloatByteSize = FLOAT_FIELD_SIZE;


    // OpenCL context
    final cl_device_id[] devicesIds = new cl_device_id[]{deviceId};
    final cl_context context = clCreateContext(null, 1, devicesIds, null, null, null);


    // OpenCL kernel
    String options = "-I../../OpenCL/";

    if (debug) {
      System.err.println("OpenCL in DEBUG mode!");
      System.err.flush();
    } else {
      options += " -DNDEBUG";
    }

    final String kernelFilename = "../kernel/example.cl";
    final String kernelSrc = fileToString(kernelFilename);
    final cl_program program = clCreateProgramWithSource(context,
                                                         1, new String[]{kernelSrc},
                                                         null, null);

    clBuildProgram(program, 1, devicesIds, options, null, null);

    final cl_kernel kernel = clCreateKernel(program, "example", null);


    // OpenCL queue
    final cl_command_queue queue = clCreateCommandQueue(context, deviceId,
                                                        CL_QUEUE_PROFILING_ENABLE, null);


    // OpenCL buffers
    final cl_mem dOuts = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR,
                                        hOutsByteSize, Pointer.to(hOuts), null);

    cl_mem dAsserts = null;
    cl_mem dAssertFloat = null;

    if (debug) {
      dAsserts = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                hAssertsByteSize, Pointer.to(hAsserts), null);
      dAssertFloat = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                    hAssertFloatByteSize, Pointer.to(hAssertFloat), null);
    }


    // Params: just two parameters for the example
    clSetKernelArg(kernel, 0, Sizeof.cl_uint, Pointer.to(new int[]{666}));
    clSetKernelArg(kernel, 1, Sizeof.cl_mem, Pointer.to(dOuts));

    if (debug) {  // extra parameters to receive assertion information
      final int nbArgs = 2;

      clSetKernelArg(kernel, nbArgs, Sizeof.cl_mem, Pointer.to(dAsserts));
      clSetKernelArg(kernel, nbArgs + 1, Sizeof.cl_mem, Pointer.to(dAssertFloat));
    }


    // Run
    final int globalSize = nbWorkItemsByWorkGroup * nbWorkGroup;

    System.out.println("===== run kernel =====");
    System.out.flush();

    clEnqueueNDRangeKernel(queue, kernel,
                           1,
                           null,
                           new long[]{globalSize},
                           new long[]{nbWorkItemsByWorkGroup},
                           0,
                           null,
                           null);

    clFinish(queue);
    clFlush(queue);
    System.out.flush();
    System.err.flush();
    System.out.println("===== end kernel =====");
    System.out.flush();


    // Results
    clEnqueueReadBuffer(queue, dOuts, CL_TRUE, 0,
                        hOutsByteSize, Pointer.to(hOuts), 0, null, null);

    if (debug) {
      clEnqueueReadBuffer(queue, dAsserts, CL_TRUE, 0,
                          hAssertsByteSize, Pointer.to(hAsserts), 0, null, null);
      clEnqueueReadBuffer(queue, dAssertFloat, CL_TRUE, 0,
                          hAssertFloatByteSize, Pointer.to(hAssertFloat), 0, null, null);

      final long line = hAsserts[0];

      if (line != 0) {  // there had (at least) an assertion
        final BigInteger uint64Value = uint64(hAsserts[1]);
        final long sint64Value = hAsserts[1];
        final float floatValue = hAssertFloat[0];

        System.err.println(String.format("%s:%d\tAssertion failed | Maybe\t%s\t%d | Maybe\t%f",
                                         kernelFilename, line,
                                         uint64Value, sint64Value, floatValue));
        /*
          Maybe incoherent assert information because the parallel execution of work items.
          But each element of these information concern assertion(s) that failed.
        */
        System.err.flush();
      }
    }

    System.out.println(String.format("Results: (%d, %d)", hOuts[0], hOuts[1]));


    // Free OpenCL resources
    clReleaseMemObject(dOuts);
    if (debug) {
      clReleaseMemObject(dAsserts);
      clReleaseMemObject(dAssertFloat);
    }

    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseContext(context);
  }


  /** \brief
   * Return in a long the value of n considered as an unsigned integer on 32 bits.
   */
  static long uint32(int n) {
    return (n >= 0
            ? n
            : 4294967296L + n);  // 2^32 + n
  }


  /** \brief
   * Return in a BigInteger the value of n considered as an unsigned integer on 64 bits.
   */
  static BigInteger uint64(long n) {
    final Long longN = n;
    final BigInteger bigN = new BigInteger(longN.toString());

    return (n >= 0
            ? bigN
            : new BigInteger("18446744073709551616").add(bigN));  // 2^64 + n
  }



  /** \brief
   * Get the optional parameter --device platform:device
   * and run the kernel ../kernel/example.cl .
   */
  public static void main(String[] args) throws Exception {
    CL.setExceptionsEnabled(true);  // enable exceptions when error with OpenCL


    boolean debug = assertsEnabled;
    int deviceI = 0;
    int platformI = 0;

    // Read parameters
    {
      int i = 0;

      while (i < args.length) {
        final String arg = args[i];

        if ("--debug".equals(arg) || "--ndebug".equals(arg)) {
          debug = "--debug".equals(arg);
        } else if ("--device".equals(arg)) {
          ++i;

          if (i >= args.length) {
            throw new Exception("Missing parameter");
          }

          final String[] bothI = args[i].split(":");

          platformI = Integer.parseInt(bothI[0]);
          if (bothI.length >= 2) {
            deviceI = Integer.parseInt(bothI[1]);
          }

          if ((platformI < 0) || (deviceI < 0)) {
            throw new Exception("Wrong parameter: " + args[i]);
          }
        }
        ++i;
      }
    }

    // Get wanted device
    final cl_device_id deviceId = getDeviceId(platformI, deviceI);

    // Get device name and print
    System.out.println(String.format("Device %d:%d %s",
                                     platformI, deviceI,
                                     getDeviceInfoString(deviceId, CL_DEVICE_NAME)));
    System.out.flush();

    // Run
    runExample(3, 4, deviceId, debug);
  }
}
