Checking example.c ...
Defines: 
Includes:
Platform:unix64
Checking example.c: NDEBUG...
(information) Cppcheck cannot find all the include files. Cppcheck can check the code without the include files found. But the results will probably be more accurate if all the include files are found. Please check your project's include directories and add all of them as include directories for Cppcheck. To see what files Cppcheck cannot find use --check-config.
==============================
example.c:44:  Include the directory when naming .h files  [build/include_subdir] [4]
example.c:134:  Almost always, snprintf is better than strcpy  [runtime/printf] [4]
example.c:192:  Never use sprintf. Use snprintf instead.  [runtime/printf] [5]
example.c:318:  Never use sprintf. Use snprintf instead.  [runtime/printf] [5]
example.c:325:  Almost always, snprintf is better than strcat  [runtime/printf] [4]
example.c:332:  Never use sprintf. Use snprintf instead.  [runtime/printf] [5]
Done processing example.c
--------------------------------------------------
Total errors found: 6
