/* -*- coding: latin-1 -*- */
/** \file examples/Cpp/example.cpp
 * \brief
 * Simple C++ example to show how run a kernel with assert*() and PRINT*() macros
 * and test them.
 *
 * Piece of assertOpenCL
 * --- GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * --- http://www.opimedia.be/
 * --- September 19, 2018
 */

// \cond
#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <map>
#include <string>

#include <CL/cl.hpp>
// \endcond



/* **********
 * Constant *
 ************/

/** \brief
 * List of all error codes and names extracted from /usr/include/CL/cl.h
 */
const std::map<int32_t, std::string> errors_map {
#include "opencl_errors_map.hpp"
};



/* ************
 * Prototypes *
 **************/

/** \brief
 * Return the directory part of path.
 */
std::string
dirname(std::string path);


/** \brief
 * Return the error name corresponding to the error code.
 */
std::string
error_name(cl_int code);


/** \brief
 * Read the file and return its content to a string.
 * If failed then print a error message and exit.
 */
std::string
file_to_string(std::string filename);


/** \brief
 * Return the given device of the given platform OpenCL,
 * or exit if doesn't exists.
 *
 * @param platform_i
 * @param device_i
 *
 * @return OpenCL device
 */
cl::Device
get_device(unsigned int platform_i, unsigned int device_i);


/** \brief
 * If code != 0
 * then print an error message corresponding to the error code.
 */
void
print_error(cl_int code, std::string message = "");


/** \brief
 * Run the kernel ../kernel/example.cl.

 * If debug
 * then run the kernel in debug mode,
 * else run the kernel with the macro NDEBUG defined.
 */
void
run_example(unsigned int nb_work_group, unsigned int nb_work_items_by_work_group,
            cl::Device device, bool debug, const std::string& path);



/* ***********
 * Functions *
 *************/

std::string
error_name(cl_int code) {
  const auto it = errors_map.find(code);

  return (it == errors_map.cend()
          ? "unknow"
          : it->second);
}


std::string
file_to_string(std::string filename) {
  std::ifstream infile(filename, std::ios::in);
  const std::string s((std::istreambuf_iterator<char>(infile)),
                      std::istreambuf_iterator<char>());

  infile.close();
  if (infile.fail()) {
    std::cerr << "! Impossible to read file \"" << filename << '"' << std::endl;

    exit(EXIT_FAILURE);
  }

  return "#line 1 \"" + filename + "\"\n" + s;
}


cl::Device
get_device(unsigned int platform_i, unsigned int device_i) {
  std::vector<cl::Platform> platforms;

  cl::Platform::get(&platforms);
  if (platform_i >= platforms.size()) {
    exit(EXIT_FAILURE);
  }

  std::vector<cl::Device> devices;

  platforms[platform_i].getDevices(CL_DEVICE_TYPE_ALL, &devices);
  if (device_i >= devices.size()) {
    exit(EXIT_FAILURE);
  }

  return devices[device_i];
}


std::string
dirname(std::string path) {
  const char sep = '/';

  if (path.empty() || (path.back() == sep)) {  // path is empty or a directory name
    return path;
  }
  else {                                       // path has the structure of a filename
    const size_t j = path.rfind(sep);

    return (j == std::string::npos
            ? ""
            : path.substr(0, j + 1));
  }
}


void
print_error(cl_int code, std::string message) {
  if (code != 0) {
    if (!message.empty()) {
      message = " " + message;
    }

    std::cerr << "! OpenCL error "
              << code << ' ' << error_name(code)
              << message
              << std::endl;
    std::cerr.flush();
  }
}


void
run_example(unsigned int nb_work_group, unsigned int nb_work_items_by_work_group,
            cl::Device device, bool debug, const std::string& path) {
  // Host buffer
  uint32_t h_outs[2];
  const size_t h_outs_byte_size = sizeof(h_outs[0])*2;

  uint64_t h_asserts[2] = {0, 0};
  const size_t h_asserts_byte_size = sizeof(h_asserts[0])*2;

  float h_assert_float[1] = {0};
  const size_t h_assert_float_byte_size = sizeof(h_assert_float[0]);

  assert(sizeof(float) == 4);


  // OpenCL context
  const cl::Context context{device};


  // OpenCL kernel
  std::string options = "-I" + path + "../../OpenCL/";

  if (debug) {
    std::cerr << "OpenCL in DEBUG mode!" << std::endl;
    std::cerr.flush();
  }
  else {        // transmits NDEBUG macro to kernel
    options += " -DNDEBUG";
  }

  const std::string kernel_filename = path + "../kernel/example.cl";
  const std::string kernel_src(file_to_string(kernel_filename));
  const cl::Program program{context, kernel_src};
  const VECTOR_CLASS<cl::Device> devices = {device};
  cl_int error = program.build(devices, options.c_str());

  print_error(error, "clBuildProgram");

  cl::Kernel kernel{program, "example"};


  // OpenCL queue
  const cl::CommandQueue queue{context, device, CL_QUEUE_PROFILING_ENABLE};


  // OpenCL buffers
  cl::Buffer d_outs{context, CL_MEM_WRITE_ONLY, h_outs_byte_size};

  cl::Buffer d_asserts{context, CL_MEM_READ_WRITE, h_asserts_byte_size};
  cl::Buffer d_assert_float{context, CL_MEM_READ_WRITE, h_assert_float_byte_size};

  if (debug) {
    error = queue.enqueueWriteBuffer(d_asserts, CL_TRUE, 0, h_asserts_byte_size, h_asserts);
    print_error(error, "clEnqueueWriteBuffer(d_asserts, ...)");

    error = queue.enqueueWriteBuffer(d_assert_float, CL_TRUE, 0, h_assert_float_byte_size, h_assert_float);
    print_error(error, "clEnqueueWriteBuffer(d_assert_float, ...)");
  }


  // Params: just two parameters for the example
  error = kernel.setArg(0, 666);
  print_error(error, "clSetKernelArg(0, ...)");

  error = kernel.setArg(1, d_outs);
  print_error(error, "clSetKernelArg(1, ...)");

  if (debug) {  // extra parameters to receive assertion information
    const unsigned int nb_args = 2;

    error = kernel.setArg(nb_args, d_asserts);
    print_error(error, "clSetKernelArg(..., d_asserts)");

    error = kernel.setArg(nb_args + 1, d_assert_float);
    print_error(error, "clSetKernelArg(..., d_assert_float)");
  }


  // Run
  const unsigned int global_size = nb_work_items_by_work_group * nb_work_group;

  std::cout << "===== run kernel =====" << std::endl;
  std::cout.flush();

  queue.enqueueNDRangeKernel(kernel,
                             cl::NullRange,
                             cl::NDRange(global_size),
                             cl::NDRange(nb_work_items_by_work_group),
                             nullptr,
                             nullptr);

  queue.finish();
  queue.flush();
  std::cout.flush();
  std::cerr.flush();
  std::cout << "===== end kernel =====" << std::endl;
  std::cout.flush();


  // Results
  error = queue.enqueueReadBuffer(d_outs, CL_TRUE, 0, h_outs_byte_size, h_outs);
  print_error(error, "clEnqueueReadBuffer(d_outs, ...)");

  if (debug) {
    error = queue.enqueueReadBuffer(d_asserts, CL_TRUE, 0, h_asserts_byte_size, h_asserts);
    print_error(error, "clEnqueueReadBuffer(d_asserts, ...)");

    error = queue.enqueueReadBuffer(d_assert_float, CL_TRUE, 0, h_assert_float_byte_size, h_assert_float);
    print_error(error, "clEnqueueReadBuffer(d_assert_float, ...)");

    const uint64_t line = h_asserts[0];

    if (line != 0) {  // there had (at least) an assertion
      const uint64_t uint64_value = h_asserts[1];
      const int64_t sint64_value = static_cast<int64_t>(h_asserts[1]);
      const float float_value = h_assert_float[0];

      std::cerr << kernel_filename << ':' << line << "\tAssertion failed | Maybe\t"
                << uint64_value << '\t' << sint64_value  << " | Maybe\t" << float_value << std::endl;
      /*
        Maybe incoherent assert information because the parallel execution of work items.
        But each element of these information concern assertion(s) that failed.
      */
      std::cerr.flush();
    }
  }

  std::cout << "Results: (" << h_outs[0] << ", " << h_outs[1] << ')' << std::endl;
}



/* ******
 * Main *
 ********/

/** \brief
 * Get the optional parameter --device platform:device
 * and run the kernel ../kernel/example.cl
 */
int
main(int argc, const char* const argv[]) {
  bool debug =
#ifdef NDEBUG
    false;
#else
    true;
#endif
  signed int device_i = 0;
  signed int platform_i = 0;

  // Read parameters
  {
    int i = 1;

    while (i < argc) {
      const std::string arg = argv[i];

      if ((arg == "--debug") || (arg == "--ndebug")) {
        debug = (arg == "--debug");
      }
      else if (arg == "--device") {
        ++i;
        if (i >= argc) {
          return EXIT_FAILURE;
        }

        const std::string both_i = argv[i];
        const size_t j = both_i.find(':');

        if (j == std::string::npos) {
          platform_i = std::stoi(both_i);
        }
        else {
          platform_i = std::stoi(both_i.substr(0, j));
          device_i = std::stoi(both_i.substr(j + 1));
        }

        if ((platform_i < 0) || (device_i < 0)) {
          return EXIT_FAILURE;
        }
      }

      ++i;
    }
  }

  // Get wanted device
  const cl::Device device = get_device(static_cast<unsigned int>(platform_i),
                                       static_cast<unsigned int>(device_i));

  // Get device name and print
  {
    std::string device_name;

    device.getInfo(CL_DEVICE_NAME, &device_name);
    if (device_name[device_name.size() - 1] == '\0') {  // remove weird null character
      device_name = device_name.substr(0, device_name.size() - 1);
    }
    std::cout << "Device " << platform_i << ':' << device_i << ' ' << device_name << std::endl;
    std::cout.flush();
  }

  // Run
  run_example(3, 4, device, debug, dirname(argv[0]));

  return EXIT_SUCCESS;
}
