#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Simple Python (2 and 3) example
to show how run a kernel with assert*() and PRINT*() macros
and test them.

Piece of assertOpenCL

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: September 19, 2018
"""

from __future__ import division
from __future__ import print_function

import os.path
import sys

import numpy as np
import pyopencl as cl  # https://mathema.tician.de/software/pyopencl/


def get_device(platform_i, device_i):
    """
    Return the given device of the given platform OpenCL.

    :param platform_i: int >= 0
    :param device_i: int >= 0

    :return: None or OpenCL device
    """
    assert isinstance(platform_i, int), type(platform_i)
    assert platform_i >= 0, platform_i

    assert isinstance(device_i, int), type(device_i)
    assert device_i >= 0, device_i

    platforms = cl.get_platforms()
    if platform_i >= len(platforms):
        return None

    devices = platforms[platform_i].get_devices()
    if device_i >= len(devices):
        return None

    return devices[device_i]


def run_example(nb_work_group, nb_work_items_by_work_group,
                device=None, debug=__debug__):
    """
    Run the kernel ../kernel/example.cl.

    If device is None
    then use the default device,
    else use device.

    If debug
    then run the kernel in debug mode,
    else run the kernel with the macro NDEBUG defined.

    :param nb_work_group: int > 0
    :param nb_work_items_by_work_group: int > 0
    :param device: None or OpenCL device
    :param debug: bool
    """
    assert isinstance(nb_work_group, int), type(nb_work_group)
    assert nb_work_group > 0, nb_work_group

    assert isinstance(nb_work_items_by_work_group, int), \
        type(nb_work_items_by_work_group)
    assert nb_work_items_by_work_group > 0, nb_work_items_by_work_group

    assert isinstance(debug, bool), type(debug)

    # Host buffer
    h_outs = np.empty(2).astype(np.uint32)

    # OpenCL context
    context = (cl.create_some_context() if device is None
               else cl.Context((device, )))

    # OpenCL kernel
    path = os.path.dirname(__file__)
    options = ['-I', os.path.join(path, '../../OpenCL/')]
    if debug:
        print('OpenCL in DEBUG mode!', file=sys.stderr)
        sys.stderr.flush()
    else:      # transmits NDEBUG macro to kernel
        options.extend(('-D', 'NDEBUG'))

    kernel_filename = os.path.join(path, '../kernel/example.cl')
    kernel_src = ('#line 1 "{}"\n{}'
                  .format(kernel_filename,
                          open(os.path.join(path, '../kernel/example.cl'))
                          .read()))
    program = cl.Program(context, kernel_src).build(options=options)

    # OpenCL queue
    queue = cl.CommandQueue(context,
                            properties=cl.command_queue_properties
                            .PROFILING_ENABLE)

    # OpenCL buffer
    mf = cl.mem_flags
    d_outs = cl.Buffer(context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                       hostbuf=h_outs)

    # Params
    type_params = [np.uint32, None]
    params = [666, d_outs]  # just two parameters for the example
    if debug:  # add extra parameters to receive assertion information
        h_asserts = np.zeros(2).astype(np.uint64)
        h_assert_float = np.zeros(1).astype(np.float32)

        d_asserts = cl.Buffer(context, mf.READ_WRITE | mf.COPY_HOST_PTR,
                              hostbuf=h_asserts)
        d_assert_float = cl.Buffer(context, mf.READ_WRITE | mf.COPY_HOST_PTR,
                                   hostbuf=h_assert_float)

        type_params.extend((None, None))
        params.extend((d_asserts, d_assert_float))

    # Run
    global_size = nb_work_items_by_work_group * nb_work_group
    kernel_f = program.example
    kernel_f.set_scalar_arg_dtypes(type_params)
    print('===== run kernel =====')
    sys.stdout.flush()

    kernel_f(queue, (global_size, ), (nb_work_items_by_work_group, ), *params)

    queue.finish()
    queue.flush()
    sys.stdout.flush()
    sys.stderr.flush()
    print('===== end kernel =====')
    sys.stdout.flush()

    # Results
    cl.enqueue_copy(queue, h_outs, d_outs)

    if debug:  # get assertion information
        cl.enqueue_copy(queue, h_asserts, d_asserts)
        cl.enqueue_copy(queue, h_assert_float, d_assert_float)

        line = int(h_asserts[0])
        if line != 0:  # there had (at least) an assertion
            uint64_value = int(h_asserts[1])
            sint64_value = int(h_asserts[1].astype(np.int64))
            float_value = float(h_assert_float[0])
            print('{}:{}\tAssertion failed | Maybe\t{}\t{} | Maybe\t{}'
                  .format(kernel_filename, line,
                          uint64_value, sint64_value,
                          float_value),
                  file=sys.stderr)
            # Maybe incoherent assert information
            # because the parallel execution of work items.
            # But each element of these information
            # concern assertion(s) that failed.
            sys.stderr.flush()

    print('Results:', tuple(h_outs))


########
# Main #
########
def main():
    """
    Get the optional parameter --device platform:device
    and run the kernel ../kernel/example.cl
    """
    debug = __debug__
    device = None

    # Read parameters
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        if (arg == '--debug') or (arg == '--ndebug'):
            debug = (arg == '--debug')
        elif arg == '--device':
            i += 1
            try:
                both_i = [arg for arg in sys.argv[i].split(':')]
                platform_i = int(both_i[0])
                device_i = (int(both_i[1]) if len(both_i) >= 2
                            else 0)
                if ((platform_i >= 0) and (device_i >= 0) and
                        (len(both_i) <= 2)):
                    device = get_device(platform_i, device_i)
                else:
                    exit(1)
            except (IndexError, ValueError):
                exit(1)

        i += 1

    # Print wanted device
    if device is None:
        print('Device default')
    else:
        print('Device {}:{} {}'.format(platform_i, device_i, device.name))
    sys.stdout.flush()

    # Run
    run_example(3, 4, device=device, debug=debug)

if __name__ == '__main__':
    main()
