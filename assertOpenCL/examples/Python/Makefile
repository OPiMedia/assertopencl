# Static checking of Python source and tests.
#
# Piece of assertOpenCL
# GPLv3 --- Copyright (C) 2018 Olivier Pirson
# http://www.opimedia.be/
# September 19, 2018

.SUFFIXES:

PEP8      = pep8
PEP8FLAGS = -v --statistics

PYLINT      = pylint3
PYLINTFLAGS =

PYTHON2      = python2
PYTHON2FLAGS =

PYTHON3      = python3
PYTHON3FLAGS =

ECHO  = echo
SHELL = bash



###
# #
###
.PHONY: lint pep8 pylint run run2 run3 test test2 test3

lint:
	-$(PEP8) $(PEP8FLAGS) *.py > log/lint.txt 2>&1
	$(ECHO) '==============================' >> log/lint.txt
	-$(PYLINT) $(PYLINTFLAGS) *.py >> log/lint.txt 2>&1

pep8:
	-$(PEP8) $(PEP8FLAGS) *.py

pylint:
	-$(PYLINT) $(PYLINTFLAGS) *.py

run:	run2 run3

run2:
	$(PYTHON2) $(PYTHON2FLAGS) example.py --device 0:0

run3:
	$(PYTHON3) $(PYTHON3FLAGS) example.py --device 0:0

test:	test2 test3

test2:
	$(PYTHON2) $(PYTHON2FLAGS) example.py > log/example_2.txt 2>&1
	$(PYTHON2) $(PYTHON2FLAGS) example.py --device 0:0 > log/example_2_0.txt 2>&1
	$(PYTHON2) $(PYTHON2FLAGS) example.py --device 1:0 > log/example_2_1.txt 2>&1
	$(PYTHON2) $(PYTHON2FLAGS) -O example.py > log/example_2_NDEBUG.txt 2>&1
	$(PYTHON2) $(PYTHON2FLAGS) -O example.py --device 0:0 > log/example_2_NDEBUG_0.txt 2>&1
	$(PYTHON2) $(PYTHON2FLAGS) -O example.py --device 1:0 > log/example_2_NDEBUG_1.txt 2>&1

test3:
	$(PYTHON3) $(PYTHON3FLAGS) example.py > log/example_3.txt 2>&1
	$(PYTHON3) $(PYTHON3FLAGS) example.py --device 0:0 > log/example_3_0.txt 2>&1
	$(PYTHON3) $(PYTHON3FLAGS) example.py --device 1:0 > log/example_3_1.txt 2>&1
	$(PYTHON3) $(PYTHON3FLAGS) -O example.py > log/example_3_NDEBUG.txt 2>&1
	$(PYTHON3) $(PYTHON3FLAGS) -O example.py --device 0:0 > log/example_3_NDEBUG_0.txt 2>&1
	$(PYTHON3) $(PYTHON3FLAGS) -O example.py --device 1:0 > log/example_3_NDEBUG_1.txt 2>&1
