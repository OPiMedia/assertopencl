Checking OpenCL/assert.cl ...
Defines: 
Includes:
Platform:unix64
Checking OpenCL/assert.cl: NDEBUG...
[OpenCL/assert.cl]: (information) The configuration 'NDEBUG' was not checked because its code equals another one.
1/3 files checked 68% done
--------------------------------------------------
Checking OpenCL/print.cl ...
Defines: 
Includes:
Platform:unix64
Checking OpenCL/print.cl: NDEBUG...
[OpenCL/print.cl]: (information) The configuration 'NDEBUG' was not checked because its code equals another one.
2/3 files checked 79% done
--------------------------------------------------
Checking examples/kernel/example.cl ...
Defines: 
Includes:
Platform:unix64
3/3 files checked 100% done
--------------------------------------------------
[OpenCL/assert.cl:569]: (style) The function 'assert_fct_' is never used.
[OpenCL/assert.cl:614]: (style) The function 'last_assert_fct_' is never used.
(information) Cppcheck cannot find all the include files. Cppcheck can check the code without the include files found. But the results will probably be more accurate if all the include files are found. Please check your project's include directories and add all of them as include directories for Cppcheck. To see what files Cppcheck cannot find use --check-config.
==============================
OpenCL/print.cl:0:  No copyright message found.  You should have a line: "Copyright [year] <Copyright Owner>"  [legal/copyright] [5]
OpenCL/print.cl:67:  Potential format string bug. Do printf("%s", str) instead.  [runtime/printf] [4]
Done processing OpenCL/print.cl
--------------------------------------------------
OpenCL/assert.cl:0:  No copyright message found.  You should have a line: "Copyright [year] <Copyright Owner>"  [legal/copyright] [5]
OpenCL/assert.cl:118:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:572:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:573:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:583:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:617:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:618:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
OpenCL/assert.cl:626:  Use int16/int64/etc, rather than the C type long  [runtime/int] [4]
Done processing OpenCL/assert.cl
--------------------------------------------------
examples/kernel/example.cl:12:  Do not include .cl files from other packages  [build/include] [4]
examples/kernel/example.cl:13:  Do not include .cl files from other packages  [build/include] [4]
examples/kernel/example.cl:126:  Do not include .cl files from other packages  [build/include] [4]
examples/kernel/example.cl:159:  Do not include .cl files from other packages  [build/include] [4]
examples/kernel/example.cl:176:  Do not include .cl files from other packages  [build/include] [4]
Done processing examples/kernel/example.cl
--------------------------------------------------
Total errors found: 15
