.. -*- restructuredtext -*-

============
assertOpenCL
============
**assert()** are not available in OpenCL_.
Here an implementation similar to the **classic C implementation**.
A difference is that instead to exit when an assertion failed
a message is printed (if at least OpenCL 1.2)
and the first assertion (or the last) is transmitted to the host program
(see examples of use with C/C++, Java, Scala and Python).

There are also some **PRINT()** macros to use **printf()** function
when it is available and that do nothing if not.

* `The online HTML documentation`_
* `These complete sources on Bitbucket`_
* Downloads_

.. _Downloads: https://bitbucket.org/OPiMedia/assertopencl/downloads/
.. _OpenCL: https://www.khronos.org/opencl/
.. _`The online HTML documentation`: http://www.opimedia.be/DS/online-documentations/assertOpenCL/html/
.. _`These complete sources on Bitbucket`: https://bitbucket.org/OPiMedia/assertopencl


.. warning::
   Be careful with \*_r() and \*_r0() versions of assert\*() when you have some barriers in your kernel.
   If at least one work-item reach the barrier, then all barriers have to reach it.
   See the official documentation of OpenCL to understand the constraints: barrier_.

.. _barrier: https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/barrier.html

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2018, 2020 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|assertOpenCL|

.. |assertOpenCL| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/16/1908145737-5-assertopencl-logo_avatar.png

Logo built from `evaluation - score`_.

.. _`evaluation - score`: https://www.openclipart.org/detail/237767/evaluation-score
